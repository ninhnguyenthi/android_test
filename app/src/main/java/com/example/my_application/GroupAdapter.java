package com.example.my_application;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class GroupAdapter  extends RecyclerView.Adapter {
    Activity activity ;
    List<Group> listgGroups;
    OnClickItem listener;

    public GroupAdapter(Activity activity, List<Group> listgGroups) {
        this.activity = activity;
        this.listgGroups = listgGroups;
    }

    void setOnClickItem(OnClickItem callback){
        this.listener = callback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = activity.getLayoutInflater().inflate(R.layout.activity_group_adapter,parent,false);
        GroupViewHolder holder = new GroupViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        GroupViewHolder vh = (GroupViewHolder) holder;
        Group model = listgGroups.get(position);
        vh.tvDatetime.setText(model.getDatetimeStr());
        vh.tvDescription.setText(model.getDescription());
        vh.tvDescription.setOnClickListener(v -> {
            listener.clickItem(model.getDescription());
        });
    }

    @Override
    public int getItemCount() {
        return listgGroups.size();
    }

    public class GroupViewHolder extends RecyclerView.ViewHolder {

        TextView tvDatetime;
        TextView tvDescription;

        public GroupViewHolder(@NonNull View itemView) {   // một contructer
            super(itemView);
            tvDatetime = itemView.findViewById(R.id.tvDatetime);
            tvDescription= itemView.findViewById(R.id.tvDescription);

        }

    }
}
