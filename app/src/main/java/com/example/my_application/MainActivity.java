package com.example.my_application;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity  {
    TextView tvUserName;

    private List<Group> listGroup = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvUserName = findViewById(R.id.tv_username);


        //B1: Data Source
        initData();

        //b2: Adapter
        GroupAdapter adapter = new GroupAdapter(this,listGroup);
        adapter.setOnClickItem(new OnClickItem() {
            @Override
            public void clickItem(String userName) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra(Constant.KEY_USERNAME,userName);
                startActivity(intent);

            }
        });

        //B3: Layout Manager
        RecyclerView.LayoutManager layoutManager  = new GridLayoutManager(this,1, GridLayoutManager.HORIZONTAL,false);


        //B4: Setup RecyclerView
        RecyclerView rvProduct = findViewById(R.id.rvProduct);
        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(adapter);


    }
    private void initData (){
        listGroup.add(new Group(1,"20/01/01","title","description","collor"));
        listGroup.add(new Group(2,"20/01/02","title2","description2","collor2"));
        listGroup.add(new Group(3,"20/01/02","title2","description2","collor2"));
        listGroup.add(new Group(4,"20/01/02","title2","description2","collor2"));
        listGroup.add(new Group(5,"20/01/02","title2","description2","collor2"));
    }

}