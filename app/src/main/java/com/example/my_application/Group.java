package com.example.my_application;

public class Group {
    private int img ;
    private String datetimeStr;
    private String title ;
    private String description;
    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Group() {
    }

    public Group(int img, String datetimeStr, String title, String description,String color) {
        this.img = img;
        this.datetimeStr = datetimeStr;
        this.title = title;
        this.description = description;
        this.color = color;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getDatetimeStr() {
        return datetimeStr;
    }

    public void setDatetimeStr(String datetimeStr) {
        this.datetimeStr = datetimeStr;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
